package TestUnit;

import Model.Monom;
import Model.Polynom;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class TestPolynomialCalculator {
    @Test
    public void TestPolynomialCalculator(){
        Monom monom1 = new Monom(3,1);
        Monom monom2 = new Monom(3,0);
        Monom monom3 = new Monom(10,4);

        Monom monom4 = new Monom(1,1);
        Monom monom5 = new Monom(4,2);
        Monom monom6 = new Monom(10,3);

        Polynom polynomTest1 = new Polynom();
        Polynom polynomTest2 = new Polynom();

        polynomTest1.addMonom(monom1);
        polynomTest1.addMonom(monom2);
        polynomTest1.addMonom(monom3);

        polynomTest2.addMonom(monom4);
        polynomTest2.addMonom(monom5);
        polynomTest2.addMonom(monom6);
        // p1 : 10x^4+3x+3 : p2:10x^3+4x^2+1x

        Polynom polynomAdd =polynomTest1.add(polynomTest2) ;
        assertTrue(polynomAdd.toString().equals("+10X^4+10X^3+4X^2+4X^1+3"));
        assertTrue(polynomTest1.multiply(polynomTest2).toString().equals("+100X^7+30X^4+30X^3+40X^6+12X^3+12X^2+10X^5+3X^2+3X^1"));
        Polynom polynomSub =polynomTest1.substract(polynomTest2) ;
        assertTrue(polynomSub.toString().equals("+10X^4-10X^3-4X^2+2X^1+3"));
        assertTrue(polynomTest1.derivative().toString().equals("+40X^3+3"));
        assertTrue(polynomTest1.integration().toString().equals("+2X^5+1.5X^2.0+3X^1"));

        Polynom[] p = polynomTest1.division(polynomTest2);
        String txt = "Q:"+p[0].toString() +" R:"+p[1].toString() ;
        assertTrue(txt.equals("Q:+X^1-0.4 R:+0.6X^2.0+3.4X^1.0+3"));
//Q:+X^1-0.4 R:+0.6X^2.0+3.4X^1.0+3

    }

}
