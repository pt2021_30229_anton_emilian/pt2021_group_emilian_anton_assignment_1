package Model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static sun.swing.MenuItemLayoutHelper.max;

/// *******RANDOM CODE FOR TESTING PURPOSE*********

public class TestClass {
        public static void main(String[] args )
        {
            /*
.       - Any Character Except New Line
\d      - Digit (0-9)
\D      - Not a Digit (0-9)
\w      - Word Character (a-z, A-Z, 0-9, _)
\W      - Not a Word Character
\s      - Whitespace (space, tab, newline)
\S      - Not Whitespace (space, tab, newline)

\b      - Word Boundary
\B      - Not a Word Boundary
^       - Beginning of a String
$       - End of a String

[]      - Matches Characters in brackets
[^ ]    - Matches Characters NOT in brackets
|       - Either Or
( )     - Group

Quantifiers:
*       - 0 or More
+       - 1 or More ->characht
?       - 0 or One
{3}     - Exact Number
{3,4}   - Range of Numbers (Minimum, Maximum) */
            //System.out.print("main");

           /* String s = "4X^2-6X^-1+10x^3-45x^4+10-45x^100";
            // ? optional
             //Pattern pattern = Pattern.compile("[-+]?([0-9] * \\.? [0-9]+)?(x(\\^\\[+-]?[0-9]+)?)?");
            Pattern pattern = Pattern.compile("([+-]?(?:(?:\\d+[x,X]\\^[+-]?[0-9]+)|(?:\\d+[x,X])|(?:\\d+)|(?:[x,X])))");
            Pattern pattern1 = Pattern.compile("([+-]?(?:(?:\\d+x\\^\\d+)|(?:\\d+[x,X])|(?:\\d+)|(?:x)))");

            Matcher mat = pattern.matcher(s);
            int x = 0 ;
            int coef , exp ;
            while(mat.find()){
                x++ ;
                System.out.println("Monomul " + x + ":" + mat.group(0) );
                String s1 = mat.group(0);
                int index1 = s1.indexOf('x') ;
                int index2 = s1.indexOf('X') ;
                int index =-1 ;
                if ((index1 != -1 && index2 ==-1) || (index1 == -1 && index2 !=-1))
                {
                    index = max(index1,index2);
                }
                if (index == -1) /// the monom does not contain x
                {
                    exp = 0;
                    String sub = s1.substring(0,s1.length());
                    System.out.println("sub:" + sub);
                }
                else{
                    System.out.println("index:" + index);
                    String sub = s1.substring(0,index);
                    System.out.println("sub:" + sub + "exp:" + s1.substring(index+2,s1.length()));
                }
                //System.out.println(s1.substring(0, index-1) + " " + Integer.parseInt(s1.substring(index+2,s1.length())));
                //coef = Integer.parseInt(s1.substring(0, index-1)); // extract the coefficient

                //exp = Integer.parseInt(s1.substring(index+1,s1.length())) ;// extract the exponent

                //System.out.println("coef:"+coef + "exp:" +exp);
            }
            */

            Polynom p1 = new Polynom() ;
            Polynom p2 = new Polynom() ;
            Polynom p3 = new Polynom() ;
            p1.createPolynom("4X^2-6X^-1+10x^3-45x^4+10-45x^100+5x^2+5x^2+5x^3");
            p2.createPolynom("4X^2-6X^-1+10x^3-45x^4+10-45x^100+5x^2+5x^2+5x^3");
            p3 = p1.add(p2);

            Polynom p4 = new Polynom() ;
            Polynom p5 = new Polynom() ;
            Polynom p6 = new Polynom() ;
            String s = "5x";
            p4.createPolynom(s);
            p5.createPolynom("6x");
            p6 = p4.multiply(p5);

            Polynom pD = p1.derivative();
            double g = p1.getGrade();
            System.out.println(g);


            Polynom a1 = new Polynom() ;
            a1.createPolynom("3x^3-2x^2+5");

            System.out.println("aa a "+a1.toString());
            Polynom a2 = new Polynom() ;

            a2.createPolynom("1x^2-1");

             System.out.println("aa a "+a2.toString());
           // a2.setPolynom(a1.getPolynom());
           // System.out.println(a2.toString());

            Polynom [] vec = new Polynom[2];
            vec =  a1.division(a2);

        }
}

