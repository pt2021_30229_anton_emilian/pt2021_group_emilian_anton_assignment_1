package Model;

public class Monom implements Comparable{
    private double coefficient ;
    private double exponent ;
    public Monom(double coef , double exp){
        this.exponent = exp ;
        this.coefficient = coef ;
    }
    public double getCoefficient() {
        return coefficient;
    }
    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }
    public double getExponent() {
        return exponent;
    }
    public void setExponent(double exponent) {
        this.exponent = exponent;
    }
    @Override
    public String toString(){
        if (exponent == (int)exponent && coefficient == (int)coefficient)
        {
            int exponent1 , coefficient1 ;
            exponent1 = (int)exponent ;
            coefficient1 =(int) coefficient ;
            if (exponent == 0)
                return ""+coefficient1 ;
            else{
                if (coefficient == 1)
                    return ""+"X^"+exponent1;
                else if (coefficient == -1)
                    return ""+"-X^"+exponent1;
                else
                    return coefficient1+"X^"+exponent1;
            }
        }
        else{
            if (exponent == 0)
                return ""+String.format("%.1f",coefficient) ;
            else{
                if (coefficient == 1)
                    return ""+"X^"+exponent;
                else if (coefficient == -1)
                    return ""+"-X^"+exponent;
                else
                    return String.format("%.1f",coefficient) +"X^"+String.format("%.1f",exponent) ;
            }
        }
    }
    @Override
    public int compareTo(Object o) {
        Monom c = (Monom)o;
        if(this.exponent == c.getExponent())
            return 0;
        else if(this.exponent > c.getExponent())
                return -1 ;
        else return 1;
    }
}
