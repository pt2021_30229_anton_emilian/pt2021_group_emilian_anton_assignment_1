package Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Math.abs;
import static sun.swing.MenuItemLayoutHelper.max;

public class Polynom {
    private ArrayList<Monom> polynom = new ArrayList<>();
    public void createPolynom(String text) {
        Pattern pattern = Pattern.compile("([+-]?(?:(?:\\d+[x,X]\\^[+-]?[0-9]+)|(?:\\d+[x,X])|(?:\\d+)|(?:[x,X])))");
        Matcher matcher = pattern.matcher(text);
        System.out.println("Create Poly");
        double coef = 0, exp = 0;
        while(matcher.find())
        {
            String stringMonom = matcher.group(0);
            int index1 = stringMonom.indexOf('x');
            int index2 = stringMonom.indexOf('X');
            int index = -1;
            if ((index1 != -1 && index2 == -1) || (index1 == -1 && index2 != -1))
            {
                index = max(index1, index2);
            }

            if (index == -1) /// the monom does not contain x
            {
                exp = 0;
                coef = Integer.parseInt(stringMonom); // the whole number
               // System.out.println("coef:" + coef + "exp:" + exp);
                this.addMonom(coef, exp);
            }
            else {
                String stringCoef = stringMonom.substring(0,index) ; // extract the coefficient
                coef = Integer.parseInt(stringCoef);
                if (index+2 >= stringMonom.length()) // in case of X^1 written X
                {
                    exp = 1 ;
                    //System.out.println("1coef:" + coef + "1exp:" + exp);
                }
                else {
                    String stringExp = stringMonom.substring(index + 2, stringMonom.length());
                    //System.out.println("coef:" + stringCoef);
                    //System.out.println("Exp:" + stringExp);
                    exp = Integer.parseInt(stringExp);// extract the exponent
                   // System.out.println("coef:" + coef + "exp:" + exp);
                }
                this.addMonom(coef, exp);
            }

        }
        }
    public double getGrade(){
        double grad = this.getMonom(0).getExponent();
        return grad;
    }
    public void addMonom(double coef , double exp){
        Monom newMonom = new Monom(coef , exp);
        this.polynom.add(newMonom);
    }
    public void addMonom(Monom newMonom){
        this.polynom.add(newMonom);
    }
    public void removeMonom(int index){
        this.polynom.remove(index);
    }
    public Monom getMonom(int index)
    {
        return polynom.get(index);
    }
    public void eliminateSameExponents(){
        Collections.sort(this.getPolynom()); /// to have a*x^n + b*x^(n-1) + ... +x^0
        int j,i = 0 ;
        for (i = 0 ; i < polynom.size()-1 ; i++) {
            for ( j = i+1 ; j < polynom.size() ; j++){
                if (polynom.get(i).getExponent() == polynom.get(j).getExponent()) {
                    double newCoef ;
                    newCoef = polynom.get(i).getCoefficient() + polynom.get(j).getCoefficient(); // we add the same exponent terms
                    polynom.get(i).setCoefficient(newCoef);
                    this.polynom.remove(j);
                    this.getMonom(i).setCoefficient(newCoef);
                    j--;//to not miss the next term
                    if (polynom.get(i).getCoefficient() == 0) {
                        this.polynom.remove(i);
                    }
                }
            }
        }
        if (polynom.size() == 0)
        {

            Monom newMonom = new Monom(0,0);
            polynom.add(newMonom);
        }
    }
    public Polynom add(Polynom p){
        System.out.println("Addition");
        System.out.println("P1:" + this.toString() + "\n" + "P2:" + p.toString());
        Polynom outPolynom = new Polynom();
        p.eliminateSameExponents();
        this.eliminateSameExponents();
        ArrayList<Monom> monomList = p.getPolynom();//copy of an arraylist of monoms
        System.out.println("aP1:" + this.toString() + "\n" + "aP2:" + p.toString());
        /// put together all the monoms
        for (Monom m : monomList){
            Monom newMonom = new Monom(m.getCoefficient(),m.getExponent());
            outPolynom.addMonom(newMonom);
        }
        for (Monom m : polynom){
            Monom newMonom = new Monom(m.getCoefficient(),m.getExponent());
            outPolynom.addMonom(newMonom);
        }
        /// now we eliminate terms with same exponents, resulting addition
        //System.out.println("eP1:" + this.toString() + "\n" + "eP2:" + p.toString());
        outPolynom.eliminateSameExponents(); //
        //System.out.println("P1:" + this.toString() + "\n" + "P2:" + p.toString());
        System.out.println("Final polynom:" + outPolynom.toString());
        return outPolynom;
    }
    public Polynom substract(Polynom p){
        //p1-p2
        System.out.println("Subtraction");
        System.out.println("P1:" + this.toString() + "\n" + "P2:" + p.toString());
        Polynom outPolynom = new Polynom();
        p.eliminateSameExponents();
        this.eliminateSameExponents();
        ArrayList<Monom> monomList = p.getPolynom();//copy of an arraylist of monoms from second operand

        for (Monom m : monomList){
            Monom newMonom = new Monom(m.getCoefficient(),m.getExponent());
            newMonom.setCoefficient((-1)*newMonom.getCoefficient()); // switching the sign of coefficient eg: -(a+b+c)
            outPolynom.addMonom(newMonom);
        }
        for (Monom m : polynom){
            Monom newMonom = new Monom(m.getCoefficient(),m.getExponent());
            outPolynom.addMonom(newMonom);
        }
        /// now we eliminate terms with same exponents, performing addition
        outPolynom.eliminateSameExponents(); //

        System.out.println("Final polynom:" + outPolynom.toString());
        return outPolynom;
    }
    public Polynom multiply(Polynom p){
        System.out.println("Multiplication");
        System.out.println("P1:" + this.toString() + "\n" + "P2:" + p.toString());
        int  i , j ;
        Polynom outPolynom = new Polynom();
        p.eliminateSameExponents();
        this.eliminateSameExponents();
        ArrayList<Monom> monomList = p.getPolynom();
        for(i = 0 ; i < monomList.size();i++)
        {
            for(j = 0 ; j < polynom.size() ; j++)
            {
                double newExp = monomList.get(i).getExponent() + polynom.get(j).getExponent();
                double newCoef =monomList.get(i).getCoefficient() * polynom.get(j).getCoefficient(); ;
                Monom newMonom = new Monom(newCoef , newExp);
                outPolynom.addMonom(newMonom);
            }
        }
        System.out.println("Final polynom:" + outPolynom.toString());
        return outPolynom ;
    }
    public Polynom derivative(){
        System.out.println("Derivative");
        System.out.println("Initial polynom:" + this.toString());
        Polynom outPolynom = new Polynom();
        this.eliminateSameExponents();
        ArrayList<Monom> monomList = polynom;
        for (Monom m : monomList)
        {
            double newCoef = m.getCoefficient();
            double newExp = m.getExponent() ;
            newCoef = newCoef * newExp ;
            newExp = newExp-1;
            if (newExp != -1){/// to eliminate free terms
                Monom newMonom = new Monom(newCoef , newExp);
                outPolynom.addMonom(newMonom);
            }
        }
        System.out.println("Final polynom:" + outPolynom.toString());
        return outPolynom ;
    }
    public Polynom integration() {
        String text="" ;
        System.out.println("Integration");
        System.out.println("Initial polynom:" + this.toString());
        int  i  ;
        Polynom outPolynom = new Polynom();
        this.eliminateSameExponents();
        ArrayList<Monom> monomList = polynom;

        for(Monom m : monomList)
        {
            double  newExp = m.getExponent() ;
            newExp = newExp+1;
            double newCoef = m.getCoefficient() / newExp;
            if (newExp != -1)
            {/// to eliminate free terms
                Monom newMonom = new Monom(newCoef , newExp);
                outPolynom.addMonom(newMonom);
            }
        }
        System.out.println("Final polynom:" + outPolynom.toString());
        return outPolynom ;
    }
    public Polynom[] division (Polynom p) {
        Polynom thisCopy = new Polynom();
        thisCopy.setPolynom(polynom);
        System.out.println("Division");
        System.out.println("P1:" + thisCopy.toString() + " \n" + "P2:" + p.toString());
        this.eliminateSameExponents();
        p.eliminateSameExponents();
        Polynom [] arrayPoly = new Polynom[2];
        arrayPoly[0] = new Polynom();
        arrayPoly[1] = new Polynom();
        if (this.getGrade() < p.getGrade()) {
            Polynom qPoly = new Polynom();
            arrayPoly[0] = qPoly ;
            arrayPoly[1] = p ;
            return  arrayPoly;
        }
        else {
            while (thisCopy.getGrade() >= p.getGrade()) {
                Monom qMonom = new Monom(thisCopy.getMonom(0).getCoefficient()/p.getMonom(0).getCoefficient(),thisCopy.getMonom(0).getExponent() - p.getMonom(0).getExponent());
                Polynom qPoly = new Polynom();
                qPoly.addMonom(qMonom);
                arrayPoly[0].addMonom(qMonom);
                Polynom auxMultiply = new Polynom();
                auxMultiply = p.multiply(qPoly);
                Polynom auxSubstract = new Polynom();
                auxSubstract = thisCopy.substract(auxMultiply);
                thisCopy.setPolynom(auxSubstract.getPolynom()); // Q reset
                arrayPoly[1].setPolynom(thisCopy.getPolynom()); // Remainder
                System.out.println("q"+arrayPoly[0].toString()+"r"+arrayPoly[1].toString());
            }
        }
        System.out.println("Q:"+arrayPoly[0].toString() + " R:" + arrayPoly[1].toString());
        return  arrayPoly;
    }
    @Override
    public String toString() {
        String text = new String() ;
        for (Monom m : polynom)
        {
            if (m.getCoefficient() < 0)
                text = text + m.toString();
            else {
                text = text +"+"+ m.toString();
            }
            //System.out.println(text);
        }
        return text;
    }
    public void display() {
        for(Monom m : polynom)
        {
            System.out.println(m.toString());
        }
    }
    public ArrayList<Monom> getPolynom() {
        return polynom;
    }
    public void setPolynom(ArrayList<Monom> polynom) {
        this.polynom = polynom;
    }
}
