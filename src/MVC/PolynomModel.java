package MVC;

import Model.Polynom;

public class PolynomModel {

    private Polynom polynom1 = new Polynom();
    private Polynom polynom2 = new Polynom();
    private String text1 ;
    private String text2 ;

    public PolynomModel(){
        reset();
    }
    public void createPolynoms(String textInput1 , String textInput2){
        this.text1 = textInput1;
        this.text2 = textInput2;
       // System.out.println("s1:"+text1 + "s2:"+text2);
        polynom1.createPolynom(textInput1);
       // System.out.println("1@"+text1 + text2);
        polynom2.createPolynom(text2);
        //System.out.println("2@"+text1 + text2);
        System.out.println("First polynom:" + polynom1.toString());
        System.out.println("Second polynom:" + polynom2.toString());
    }
    public void reset(){
        text1 = "" ;
        text2 = "" ;
    }
    public Polynom getPolynom1() {
        return polynom1;
    }
    public void setPolynom1(Polynom polynom1) {
        this.polynom1 = polynom1;
    }

    public Polynom getPolynom2() {
        return polynom2;
    }
    public void setPolynom2(Polynom polynom2) {
        this.polynom2 = polynom2;
    }
    public String getText1() {
        return text1;
    }
    public void setText1(String text1) {
        this.text1 = text1;
    }
    public String getText2() {
        return text2;
    }
    public void setText2(String text2) {
        this.text2 = text2;
    }
}
