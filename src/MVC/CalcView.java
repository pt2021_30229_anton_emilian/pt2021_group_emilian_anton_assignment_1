package MVC;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class CalcView extends JFrame
{
    private JTextField polynomOneTxt = new JTextField() ;
    private JTextField polynomTwoTxt = new JTextField() ;
    private JTextField output = new JTextField() ;

    private JButton addBtn = new JButton("+");
    private JButton subBtn = new JButton("-");
    private JButton multiplyBtn = new JButton("*");
    private JButton divisionBtn = new JButton("/");
    private JButton integBtn = new JButton("Integ");
    private JButton derivBtn = new JButton("Deriv");
    private JButton clearBtn = new JButton("Clear");
    CalcView(){
        polynomOneTxt.setPreferredSize(new Dimension(140,30));
        polynomTwoTxt.setPreferredSize(new Dimension(140,30));
        output.setPreferredSize(new Dimension(140,30));
        output.setEditable(false);

        JPanel panelOne = new JPanel();
        panelOne.setBackground(new Color(32, 203, 87, 122));
        panelOne.setBounds(0 , 0 , 450 , 50 );
        panelOne.add( new JLabel("P1(x)")) ;
        panelOne.add( polynomOneTxt);

        JPanel panelTwo = new JPanel();
        panelTwo.setBackground(new Color(32, 203, 87, 122));
        panelTwo.setBounds(0 , 50 , 450 , 50 );
        panelTwo.add(new JLabel("P2(x)"));
        panelTwo.add(polynomTwoTxt);

        JPanel panelOut = new JPanel();
        panelOut.setBackground(new Color(11, 224, 78, 122));
        panelOut.setBounds(0 , 100 , 450 , 50 );
        panelOut.add(new JLabel("Pout(x)"));
        panelOut.add(output);

        JPanel panel3 = new JPanel();
        panel3.setBackground(new Color(11, 229, 23, 111));
        panel3.setBounds(0 , 150 , 450 , 50 );
        panel3.add(new JLabel("Operations:"));

        JPanel panelOp = new JPanel();
        panelOp.setBackground(new Color(40, 203, 203, 111));
        panelOp.setBounds(0 , 200 , 450 , 150 );

        JPanel panelClear = new JPanel();
        panelClear.setBackground(new Color(215, 27, 27, 181));
        panelClear.setBounds(0,350,450,50);
        panelClear.add(clearBtn);

        JPanel panelRule1 = new JPanel();
        panelRule1.setBackground(new Color(32, 203, 87, 111));
        panelRule1.setBounds(0 , 450-50 , 450 , 50 );
        panelRule1.add(new JLabel("1:We must write the coefficient for a monom. E.g 1x / 2X / 100X"));

        JPanel panelRule2 = new JPanel();
        panelRule2.setBackground(new Color(32, 203, 87, 111));
        panelRule2.setBounds(0 , 500-50 , 450 , 50 );
        panelRule2.add(new JLabel("2:Not allowed to write other symbols than:'x,X,[0-9],+,-,^' "));

        JPanel panelRule3 = new JPanel();
        panelRule3.setBackground(new Color(32, 203, 87, 111));
        panelRule3.setBounds(0 , 550-50 , 450 , 50 );
        panelRule3.add(new JLabel("3:Deriv or Integ applies only for P1(x)"));

        JPanel panelRule4 = new JPanel();
        panelRule4.setBackground(new Color(32, 203, 87, 111));
        panelRule4.setBounds(0 , 600-50 , 450 , 50 );
        panelRule4.add(new JLabel("4:Example: 5X^2+1X+5x^10-10x^2+3"));

        panelOp.add(addBtn);
        panelOp.add(subBtn);
        panelOp.add(multiplyBtn);
        panelOp.add(divisionBtn);
        panelOp.add(derivBtn);
        panelOp.add(integBtn);
        panelOp.setLayout(new GridLayout(3,3));
       // panelTwo.setLayout(new BorderLayout());

        this.setTitle("Polynomial Calculator");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(450,630);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setLayout(null);
        this.getContentPane().setBackground(new Color(245, 255, 255, 111));
       //ImageIcon image = new ImageIcon("calc2.jpg");
        //this.setIconImage(image.getImage());
        JPanel allPanels = new JPanel();
        allPanels.add(panelOne);
        allPanels.add(panelTwo);
        allPanels.add(panelOut);
        allPanels.add(panel3);
        allPanels.add(panelOp);
        allPanels.add(panelClear);
        allPanels.add(panelRule1);
        allPanels.add(panelRule2);
        allPanels.add(panelRule3);
        allPanels.add(panelRule4);
        allPanels.setLayout(null);

        this.setContentPane(allPanels);
    }
    public String getPolyOneTxt()
    {
        return polynomOneTxt.getText();
    }
    public String getPolyTwoTxt()
    {
        return polynomTwoTxt.getText();
    }
    public void setPolynomOneTxt() {
        polynomOneTxt.setText("");
    }
    public void setPolynomTwoTxt() {
        polynomTwoTxt.setText("");
    }
    public void setPolynomOutTxt() {
        output.setText("");
    }
    ///Methods
    public void displayText(String text){output.setText(text);}
    public void addAddListener(ActionListener adder){    addBtn.addActionListener(adder); }
    public void addMultiplyListener(ActionListener mul){ multiplyBtn.addActionListener(mul); }
    public void addSubListener(ActionListener sub){      subBtn.addActionListener(sub); }
    public void addDivisionListener(ActionListener div) {divisionBtn.addActionListener(div); }
    public void addDerivListener(ActionListener deriv) { derivBtn.addActionListener(deriv); }
    public void addIntegListener(ActionListener integ) { integBtn.addActionListener(integ); }
    public void clearListener(ActionListener clear) { clearBtn.addActionListener(clear); }

}
