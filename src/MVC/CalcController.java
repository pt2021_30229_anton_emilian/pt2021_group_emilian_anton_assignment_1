package MVC;

import Model.Polynom;

import java.awt.event.*;

public class CalcController {
    private CalcView view ;
    private PolynomModel model ;
    public CalcController(CalcView view , PolynomModel model ){
        this.view = view ;
        this.model = model ;

        view.addAddListener(new AddListener());
        view.addSubListener(new SubListener());
        view.addMultiplyListener(new MultiplyListener());
        view.addDivisionListener(new DivisionListener());
        view.addDerivListener(new DerivListener());
        view.addIntegListener(new IntegListener());
        view.clearListener(new ClearListener());
    }
    class AddListener implements ActionListener{
        String userInputPoly1 = "" ;
        String userInputPoly2 = "" ;
        public void actionPerformed(ActionEvent e)
        {
            userInputPoly1 = view.getPolyOneTxt();
            userInputPoly2 = view.getPolyTwoTxt();
            model.createPolynoms(userInputPoly1 , userInputPoly2);
            Polynom p1 = model.getPolynom1() ;
            Polynom p2 = model.getPolynom2() ;
            Polynom p3 = p1.add(p2);
            view.displayText(p3.toString());
            System.out.println("Result:" + p3.toString());
        }
    }
    class SubListener implements ActionListener{
        String userInputPoly1 = "" ;
        String userInputPoly2 = "" ;
        public void actionPerformed(ActionEvent e)
        {
            userInputPoly1 = view.getPolyOneTxt();
            userInputPoly2 = view.getPolyTwoTxt();
            model.createPolynoms(userInputPoly1 , userInputPoly2);
            Polynom p1 = model.getPolynom1() ;
            Polynom p2 = model.getPolynom2() ;
            Polynom p3 = p1.substract(p2);
            view.displayText(p3.toString());
            System.out.println("Result:" + p3.toString());
        }
    }
    class MultiplyListener implements ActionListener{
        String userInputPoly1 = "" ;
        String userInputPoly2 = "" ;
        public void actionPerformed(ActionEvent e)
        {
            userInputPoly1 = view.getPolyOneTxt();
            userInputPoly2 = view.getPolyTwoTxt();
            model.createPolynoms(userInputPoly1 , userInputPoly2);
            Polynom p1 = model.getPolynom1() ;
            Polynom p2 = model.getPolynom2() ;
            Polynom p3 = p1.multiply(p2);
            view.displayText(p3.toString());
        }
    }
    class DivisionListener implements ActionListener{
        String userInputPoly1 = "" ;
        String userInputPoly2 = "" ;
        public void actionPerformed(ActionEvent e)
        {
            userInputPoly1 = view.getPolyOneTxt();
            userInputPoly2 = view.getPolyTwoTxt();
            model.createPolynoms(userInputPoly1 , userInputPoly2);
            Polynom p1 = model.getPolynom1() ;
            Polynom p2 = model.getPolynom2() ;
            Polynom[] p = p1.division(p2);

            view.displayText("Q:"+p[0].toString() + " R:"+p[1].toString());
        }
    }
    class DerivListener implements ActionListener{
        String userInputPoly1 = "" ;
        String userInputPoly2 = "" ;
        public void actionPerformed(ActionEvent e)
        {
            userInputPoly1 = view.getPolyOneTxt();
            userInputPoly2 = view.getPolyTwoTxt();
            model.createPolynoms(userInputPoly1 , userInputPoly2);
            Polynom p1 = model.getPolynom1() ;
            Polynom p2 = model.getPolynom2() ;

            Polynom p3 = p1.derivative();
            view.displayText(p3.toString());
        }
    }
    class IntegListener implements ActionListener{
        String userInputPoly1 = "" ;
        String userInputPoly2 = "" ;
        public void actionPerformed(ActionEvent e)
        {
            userInputPoly1 = view.getPolyOneTxt();
            userInputPoly2 = view.getPolyTwoTxt();
            model.createPolynoms(userInputPoly1 , userInputPoly2);
            Polynom p1 = model.getPolynom1() ;
            Polynom p2 = model.getPolynom2() ;

            Polynom p3  = p1.integration();
            view.displayText(p3.toString());
        }
    }
    class ClearListener implements ActionListener{

        public void actionPerformed(ActionEvent e)
        {
            view.setPolynomOneTxt();
            view.setPolynomTwoTxt();
            view.setPolynomOutTxt();
            model.getPolynom1().getPolynom().clear();
            model.getPolynom2().getPolynom().clear();
        }
    }
}
