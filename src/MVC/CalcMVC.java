package MVC;

public class CalcMVC {
    public static void main(String[] args ){
        System.out.print("main");
        CalcView view = new CalcView();
        PolynomModel model = new PolynomModel();
        CalcController controller = new CalcController(view, model);
    }
}
